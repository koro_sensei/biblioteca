package modelo;


public abstract class Empleados {
	private String Identification;
	private String nombre;
	private String horasTrabajadas;
	
	public String getHorasTrabajadas() {
		return horasTrabajadas;
	}

	public void setHorasTrabajadas(String horasTrabajadas) {
		this.horasTrabajadas = horasTrabajadas;
	}

	public void setIdentification(String identification) {
		Identification = identification;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getIdentification() {
		return Identification;
	}
	
	public void setIdentidication(String identification) {
		Identification = identification;
	}
	
	public abstract String trabajar(String trabajo);
}
