package modelo;
import java.util.ArrayList;


public class Bibliotecarios extends Empleados implements LegislacionLaboralChile{
	
	private String librosPrestados;
	
	private ArrayList <Libro> libros = new ArrayList<>();

	
	public Bibliotecarios(){
		librosPrestados = "No se ha Prestado Ningun Libro";
	}

	@Override
	public String trabajar(String libro) {
		librosPrestados += "\n"+libro;
		return librosPrestados;
	}

	@Override
	public int calcularSalario(int horas) {
		return horas*1600;
	}
	
	public int calcularSalario(int horas, int precioXHora) {
		return horas*precioXHora;
		
	}
	public void aniadirlibro(Libro librito) {
		this.libros.add(librito);
	}


	
}
