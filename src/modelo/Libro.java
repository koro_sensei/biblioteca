package modelo;

public class Libro {
	private String titulo;
	private String anioPublicacion;
	
	public String getTitulo() {
		return titulo;
	}
	public String setTitulo(String titulo) {
		return this.titulo = titulo;
	}
	public String getAnioPublicacion() {
		return anioPublicacion;
	}
	public String setAnioPublicacion(String anioPublicacion) {
		return this.anioPublicacion = anioPublicacion;
	}
	
	
	
}
